
	// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON placeholder API
	fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify()
	})


	// 4. Using the data retrieved create an array using the map method to return just the title of every item and print the result in the console
	.then((response) => response.json())
	.then((json) => console.log(json.map(objectTitle => objectTitle.title)))


	// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API
	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify()
	})


	// 6. Using the data retrieved print a message in the console that will provide the title and status of the to do list item
	.then((response) => response.json())
	.then((json) => 
		console.log(`${json.title} ${json.completed}`))
	

	// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API
	fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			userId: 1,
			title: "To do do doooo",
			completed: false
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))
	

	// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API
	fetch('https://jsonplaceholder.typicode.com/todos/101', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			userId: 1,			
			title: 'Finished Task',
			completed: true
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

	// 9. Update a to do list item by changing the data structure to contain the following properties
	fetch('https://jsonplaceholder.typicode.com/todos/101', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			userId: 1,
			title: 'Modified structure list',
			description: 'this is a modified to do list structure',
			dateCompleted: 20211026,
			status: true
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))


	// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API
	fetch('https://jsonplaceholder.typicode.com/todos/11', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Modified List of things to do'
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

	// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.
	fetch('https://jsonplaceholder.typicode.com/todos/51', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			completed: true,
			completedOn: 20211026
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

	// 12. Create a request via Postman to retrieve all the to do list items
		/*
			a. GET HTTP method
			b. https://jsonplaceholder.typicode.com/todos URL endpoint
			c. save this request as get all to do list items

		*/

	// 13. Create a request via Postman to retrieve an individual to do list item
		/*
			a. GET HTTP method
			b. https://jsonplaceholder.typicode.com/todos/1 URL endpoint
			c. save this request as get to do list item

		*/

	// 14. Create a request via Postman to create a to do list item
		/*
			a. POST HTTP method
			b. https://jsonplaceholder.typicode.com/todos/ URL endpoint
			c. save this request as create to do list item

		*/